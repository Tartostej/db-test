package com.tejcy.dbTest.repository;

import com.tejcy.dbTest.entity.Contact;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ContactRepository extends CrudRepository<Contact, UUID> {
}
